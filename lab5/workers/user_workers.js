const User = require('../models/user');
const bot_msgs = require('./bot_msgs');

module.exports = {
    getUserInfo,
    userSignIn,
    userSignUp,
    userUpdate,
    isLoggedToday
}

function getUserInfo(userID, cb) {
    User.findById(userID, (err, user) => {
        if(user){
            cb(bot_msgs.getUserInfo(user));
        }
        else 
            cb(bot_msgs.userNotFound)
    });
}

function userSignIn(username, password, cb) {
    User.findOne({ userlogin: username }, (err, user) => {
        if (!user) 
            cb(bot_msgs.userNotFound);
        else if (password != user.password) 
            cb(bot_msgs.incorrectPassword)
        else {
            let today = new Date();
            user.lastlogin = `${today.getDate()}.${(today.getMonth() + 1)}.${today.getFullYear()}`;
            user.save();
            cb(bot_msgs.userSignedIn(user));
        }
    })
}

function userSignUp(info, cb) {
    User.findOne({ userlogin: info[0] }, (err, user) => {
        if (user) cb(bot_msgs.userExists);
        else {
            let newUser = new User();
            newUser.userlogin = info[0];
            newUser.password = info[1];
            newUser.firstName = info[2];
            newUser.middleName = info[3];
            newUser.lastName = info[4];
            newUser.birthdate = info[5];
            newUser.save((err, doc) => {
                cb(bot_msgs.signUpSuccess + doc._id);
            });
        }
    });
}

function userUpdate(userUpd, cb){
    User.findById(userUpd.id, (err, user) => {
        if(!user) cb(bot_msgs.userNotFound)
        if(userUpd.login) user.userlogin = userUpd.login;
        if(userUpd.password) user.password = userUpd.password;
        if(userUpd.firstName) user.firstName = userUpd.firstName;
        if(userUpd.middleName) user.middleName = userUpd.middleName;
        if(userUpd.lastName) user.lastName = userUpd.lastName; 
        if(userUpd.birthDate) user.birthdate = userUpd.birthDate;
        user.save();
        cb(bot_msgs.userUpdated);
    });
}

function isLoggedToday(id, cb) {
    User.findById(id, (err, user) => {
        if(!user) cb(bot_msgs.userNotFound);
        if(!user.lastlogin) cb(bot_msgs.loginNotFound);
        else {
            let today = new Date();
            const cond = user.lastlogin === `${today.getDate()}.${(today.getMonth() + 1)}.${today.getFullYear()}`
            if(cond) {
                cb(bot_msgs.userLoggedToday(id))
            } else {
                cb(bot_msgs.userNotLoggedToday(id, user.lastlogin));
            }
        }
    })    
}